#!/usr/bin/env python3

import inspect
import requests
import sys
import random
import string
from enum import Enum
from sys import argv
import json
import re
import os 
import copy


random = random.SystemRandom()
PORT = 8889
EXPLOIT_NAME = argv[0]
DEFAULT_USER = "default"
DEFAULT_PASS = "default"

os.environ['PWNLIB_NOTERM'] = '1'
argv = copy.deepcopy(sys.argv)
from pwn import *

class flag_data():
    def __init__(self, flag_id, auth_data, vuln):
        self.flag_id = flag_id
        self.auth_data = auth_data
        self.vuln = vuln
        

class user_data():
    def __init__(self, login, password, key_phrase):
        self.login = login
        self.password = password
        self.key_phrase = key_phrase

    @classmethod
    def from_str(cls, string):
        data = string.split('\t')
        data_key_value = data[0].split(': ')
        login = data_key_value[1]
        data_key_value = data[1].split(': ')
        password = data_key_value[1]
        data_key_value = data[2].split(': ')
        key_phrase = data_key_value[1]
        return cls(login, password, key_phrase)

    def __str__(self):
        string = "USER_DATA OBJECT\n"
        string += "login: " + self.login + "\npassword: " + self.password + "\nkey_phrase: " + self.key_phrase
        return string

    def __repr__(self):
        string = "USER_DATA OBJECT\n"
        string += "login: " + self.login + "\npassword: " + self.password + "\nkey_phrase: " + self.key_phrase
        return string

class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110

class CheckSession():
    LL = 1000

def _log(obj):
    if DEBUG and obj:
        caller = inspect.stack()[1].function
        print(f"[{caller}] {obj}", file = sys.stderr)
    return obj

def generate_login(a:int, b:int):
    login_length = random.randint(a, b)
    characters = string.ascii_letters + string.digits
    login = ''.join(random.choice(characters) for i in range(login_length))
    return login

def generate_pass(a:int, b:int):
    chars = string.ascii_letters + string.digits + '*+=-_?!'
    password_length = random.randint(a, b)
    password = ''.join(random.choice(chars) for _ in range(password_length))
    return password

def check(host):
    username = "username#" + generate_login(10, 20)
    key_phrase = username + "#key_phrase#" + generate_login(10, 20)
    password = username + "#password#" + generate_pass(26, 39)
    try:
        conn = remote(host, PORT)
    except PwnlibException:
        die(ExitStatus.DOWN)


    # register check
    print_blue("register check")
    conn.recvuntil(b">>\n")
    conn.sendline(b'1')
    conn.recvuntil(b">>\n")
    conn.sendline(username.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(password.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(key_phrase.encode())
    check_point_1 = conn.recvuntil(b">>\n").decode()

    print_yellow(check_point_1)

    if "Anata wa de-ta-be-su ni sonzai shimasen, ningen-chan!" in check_point_1:
        print("Error: point 1...")
        die(ExitStatus.MUMBLE)
    else:
        print("Success: point 1")
    
    # key phrase change check
    print_blue("key phrase change check")
    conn.sendline(b'2')
    conn.recvuntil(b">>\n")
    conn.sendline(username.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(password.encode())
    conn.recvuntil(b">>\n")
    conn.sendline("6".encode())
    conn.recvuntil(b">>\n")
    new_key_phrase = generate_login(10, 20)
    conn.sendline(new_key_phrase.encode())
    conn.recvuntil(b">>\n")
    conn.sendline("9".encode())
    
    conn = remote(host, PORT)
    conn.recvuntil(b">>\n")
    conn.sendline(b'3')
    conn.recvuntil(b">>\n")
    conn.sendline(username.encode())
    current_key_phrase = conn.recvline().decode().split(" ")[-1][:-1]
    if current_key_phrase != new_key_phrase:
        print("Error: point 2...")
        die(ExitStatus.CORRUPT)
    else:
        print("Success: point 2")
    
    # task check
    print_blue("task check")
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'2')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    # conn.sendline(DEFAULT_USER.encode())
    conn.sendline(username.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    # conn.sendline(DEFAULT_PASS.encode())
    conn.sendline(password.encode())
    
    # add user's information
    nick = username + "#nick#" + generate_login(10, 20)
    university = username + "#university#" + generate_login(10, 20)
    team = username + "#team#" + generate_login(10, 20)
    user_info = username + "#user_info#" + generate_login(10, 20)

    print_blue("{} : {}".format(username, nick))

    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'2')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(nick.encode())

    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(university.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(team.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(user_info.encode())
     
    # add record to db
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'1')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'3') # no important (random)
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    text = generate_login(10, 20)
    conn.sendline(text.encode())
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'0')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'5')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'7')
    db_records = conn.recvuntil(b">>\n").decode()

    print_blue("text: {}".format(text))
    print_yellow("db:\n{}".format(db_records))

    if text not in db_records:
        print("Error: point 3...")
        die(ExitStatus.CORRUPT)
    else:
        print("Success: point 3")

        # exit
    conn.sendline(b'9')
    conn.close()
    die(ExitStatus.OK)

def die(code: ExitStatus, msg: str = "data"):
    if msg:
        print(msg, file=sys.stderr)
    exit(code.value)

def registrated_user(host, flag_id, flag):
    username = generate_login(10, 20)
    password = generate_pass(26, 39)

    conn = remote(host, PORT)
    conn.recvuntil(b">>\n")
    conn.sendline(b'1')
    conn.recvuntil(b">>\n")
    conn.sendline(username.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(password.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(flag.encode())
    conn.sendline(b"4")

    jd = json.dumps({
        "flag_id": flag_id,
        "username": username,
    })

    print("PUT FLAG\nvuln: {}\nflag: {}\nflag id: {}\n".format(1, flag, username))

    return jd

def default_login(host, flag):
    conn = remote(host, PORT)

    # login
    conn.recvuntil(b">>\n")
    conn.sendline(b'2')
    conn.recvuntil(b">>\n")
    conn.sendline(DEFAULT_USER.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(DEFAULT_PASS.encode())

    # input flag
    conn.recvuntil(b">>\n")
    conn.sendline(b'1')
    conn.recvuntil(b">>\n")
    conn.sendline(b'1')
    conn.recvuntil(b">>\n")
    conn.sendline(flag.encode())
    flag_id_str = conn.recvuntil(b"\n")
    flag_id = re.search(r"\d{1,}", flag_id_str.decode()).group(0)

    # exit
    conn.recvuntil(b">>\n")
    conn.sendline(b'0')
    conn.recvuntil(b">>\n")
    conn.sendline(b'9')
    conn.close()

    # json
    jd = json.dumps({
        "id": flag_id,
    })


    print("PUT FLAG\nvuln: {}\nflag: {}\nflag id: {}\n".format(2, flag, flag_id))

    return jd


def put(host: str, flag_id: str, flag: str, vuln: int):    
    if vuln == "1":
        jd = registrated_user(host, flag_id, flag)
        die(ExitStatus.OK, f"{jd}")
    elif vuln == "2":
        jd = default_login(host, flag)
        die(ExitStatus.OK, f"{jd}")
    return

# get flag from flag store 1
def log_in(host, flag_id):

    flag_id = json.loads(flag_id)["username"]
    print("GET\nflag_id: {}".format(flag_id))

    conn = remote(host, PORT)

    conn.recvuntil(b">>\n")
    conn.sendline(b'3')
    conn.recvuntil(b">>\n")
    conn.sendline(flag_id.encode())
    flag_str = conn.recvuntil(b"\n").decode()
    data = conn.recvuntil(b'\n')
    print(data.decode())
    conn.recvuntil(b">>\n")
    conn.sendline(b"4")
    conn.close()

    flag = flag_str.split(" ")[-1][:-1]
    # flag = re.search(r"TEAM\d{3}_[A-Z0-9]{32}", flag_str)
    print("flag: {}".format(flag))
    return flag

def get_from_flagstore_2(host, flag_id):
    flag_id = json.loads(flag_id)["id"]
    conn = remote(host, PORT)

    # login
    conn.recvuntil(b">>\n")
    conn.sendline(b'2')
    conn.recvuntil(b">>\n")
    conn.sendline(DEFAULT_USER.encode())
    conn.recvuntil(b">>\n")
    conn.sendline(DEFAULT_PASS.encode())

    # get flag
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'5')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(b'2')
    data = conn.recvuntil(b">>\n")
    print(data.decode())
    conn.sendline(flag_id.encode())

    # parse flag
    flag_str = conn.recvuntil(b">>\n").decode()
    print(flag_str)
    # flag = re.search(r"team\d{1,}", flag_str)
    #flag = re.search(r"team\d{1,}", flag_str).group(0)
    try:
        flag = re.search(r"TEAM\d{3}_[A-Z0-9]{32}", flag_str).group(0)    
        print("FLAG IS: \n" + flag)
    except:
        print("None")
        flag = ''

    # exit
    conn.sendline(b'9')
    conn.close()
    return flag

def get(host: str, flag_id, flag: str, vuln: str):

    if vuln == "1":
        flag_id = json.dumps({
            "username": flag_id,
        })
        current_flag = log_in(host, flag_id)
        if flag == current_flag:
            print("success: {} == {}".format(flag, current_flag))
        else:
            print("fall: {} != {}".format(flag, current_flag))
        # die(ExitStatus.OK, f"{jd}")
    
    elif vuln == "2":
        flag_id = json.dumps({
            "id": flag_id,
        })
        current_flag = get_from_flagstore_2(host, flag_id)        
        print("current_flag: " + current_flag)
        print("flag: " + flag)
        if flag == current_flag:
            print("success: {} == {}".format(flag, current_flag))
        else:
            print("fall: {} != {}".format(flag, current_flag))
        # die(ExitStatus.OK, f"{jd}")

def info():
    print("3:1")
    return ExitStatus.OK

def info():
    print ('{"vulns": 2, "timeout": 40, "attack_data": ""}', flush=True, end="")
    die(ExitStatus.OK)

def print_blue(text):
    print("\033[1m\033[34m{}\033[0m\033[37m" .format(text))
    return

def print_yellow(text):
    print("\033[1m\033[33m{}\033[0m\033[37m" .format(text))
    return


def main():  
    mode = argv[1]
    hostname = argv[2]
    try:    
        if mode == "info":
            info()

        elif mode == "check":
            print_blue("check mode")
            check(hostname)

        elif mode == "put":
            print_blue("put mode")

            id = argv[3]
            print_blue("id = {}".format(id))
            flag = argv[4]

            print_blue("flag = {}".format(flag))
            vuln = argv[5]
            print_blue("vuln = {}".format(vuln))
            put(hostname, id, flag, vuln)

        elif mode == "get":
            print_blue("get mode")            

            id = argv[3]
            print_blue("id = {}".format(id))
            flag = argv[4]
            print_blue("flag = {}".format(flag))
            vuln = argv[5]
            print_blue("vuln = {}".format(vuln))
            put(hostname, id, flag, vuln)

            get(hostname, id, flag, vuln)
        else:
            raise IndexError
    except IndexError:
        print("GG WP!")

if __name__ == '__main__':
    main()
