# CHECKER FOR SERVICE 1

Запуск при помощи следующих команд:

./check.py <режим работы> <прочие аргументы>

./check1.py info

./check1.py check <host_ip_addr>

./check1.py put <host_ip_addr> <flag_id> <flag> <vuln_id>

./check1.py put <host_ip_addr> <flag_id> <flag> <vuln_id>

Здесь host_ip_addr - адрес, по которому будет работать проверяющая программа, flag_id - id флага, flag - флаг (нужно записывать в формате flag + 'm', 'm' нужна для того, чтобы программа нормальнот обрабатывала ввод из консоли для верхнего регистра), vuln_id - номер уязвимости.
