import random
import string
import sys
import json

class user_data():
    def __init__(self, login, password, key_phrase):
        self.login = login
        self.password = password
        self.key_phrase = key_phrase

    @classmethod
    def from_str(cls, string):
        #print(type(string))
        data = string.split('\t')
        #print(data)
        # for x in data:
        #     print('data', x)
        #     data_key_value = x.split(': ')
        #     print("=============================", x, data_key_value)
        #     if data_key_value[0] == 'login':
        #         login = data_key_value[1]
        #     elif data_key_value[0] == 'password':
        #         password = data_key_value[1]
        #     elif data_key_value[0] == 'key_phrase':
        #         key_phrase = data_key_value[1]
        #     else:
        #         print("Something is wrong!")
        #         return
        data_key_value = data[0].split(': ')
        login = data_key_value[1]
        data_key_value = data[1].split(': ')
        password = data_key_value[1]
        data_key_value = data[2].split(': ')
        key_phrase = data_key_value[1]
        return cls(login, password, key_phrase)

    def __str__(self):
        string = "USER_DATA OBJECT\n"
        string += "login: " + self.login + "\npassword: " + self.password + "\nkey_phrase: " + self.key_phrase
        return string

    def __repr__(self):
        string = "USER_DATA OBJECT\n"
        string += "login: " + self.login + "\npassword: " + self.password + "\nkey_phrase: " + self.key_phrase
        return string

def generate_login(a:int, b:int):
    # задаем длину логина
    login_length = random.randint(a, b)
    # задаем набор символов по которым будем генерировать логин
    characters = string.ascii_letters + string.digits
    # генерируем рандомный набор символов длиной равной длине логина
    login = ''.join(random.choice(characters) for i in range(login_length))
    return login

def generate_pass(a:int, b:int):
    # задаем символы, из которых будут генерироваться пароли
    chars = string.ascii_letters + string.digits + '*+=-_?!'
    # задаем длину пароля (от 10 до 15)
    password_length = random.randint(a, b)
    # генерируем пароль
    password = ''.join(random.choice(chars) for _ in range(password_length))
    return password

def flag_gen():
    #random.seed("flags_for_ctf")
    team_number = random.randint(1, 9)
    team_uuid = ''.join(random.choices(string.ascii_uppercase + string.digits, k=32))
    generated_string = f"TEAM00{team_number}_{team_uuid}"
    return generated_string

def main(num:int):
    users_list = []
    for i in range(0, num):
        U = user_data(generate_login(10, 20), generate_pass(10, 20), flag_gen())
        users_list.append(U)
    with open('my_file.txt', 'w') as file:
        for x in users_list:
            file.write("%s\n" % x)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        try:
            num = int(sys.argv[1])
            main(num)

        except ValueError:
            print("The argument should be a number")
    else:
        print("Usage: python3 flag_generator.py <num>")

    list_data = []

    with open('my_file.txt', 'r') as file:
        for line in file:
            list_data.append(line.strip())
    
    for x in range(0, len(list_data) // 3):
        data = list_data[x * 3] + '\t' + list_data[x * 3 + 1] + '\t' + list_data[x * 3 + 2]
        U = user_data.from_str(data)
        print(U)