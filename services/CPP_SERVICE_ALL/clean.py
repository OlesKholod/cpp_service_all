import subprocess

def main():
    cmd = "rm -rf ./data_bases ./users_data && make clean"
    subprocess.run(cmd, shell=True)

if __name__ == "__main__":
    main()