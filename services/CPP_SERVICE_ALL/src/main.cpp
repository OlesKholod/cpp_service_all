#include <cstdio>
#include <mutex>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>
#include <vector>
#include <locale>
#include <unistd.h>
#include <sys/file.h>
#include "DB.hpp"
#include "../libs/auth.hpp"
#include "crypto.hpp"
#include "DES.hpp"
#include "MD5.hpp"
#include <sqlite3.h>

bool is_login = false;
int current_id;
std::string nickname;

void poweroff_buff(){
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
	return;
}

void print_banner(){
    std::string line;
    
    std::ifstream in("src/banner.txt");
    if (in.is_open()){
        while (getline(in, line)){
            std::cout << line << std::endl;
        }
    } else {
        std::cout << "Hello Dio!" << std::endl;
    }
    in.close();
    return;
}

int get_id() {
    const char* filename = "./src/answer_id.txt";
    int number;
    // Открываем файл с блокировкой
    int file = open(filename, O_RDWR);
    if (file == -1) {
        std::cerr << "Не удалось открыть файл." << std::endl;
        return -1;
    }
    
    // Запрашиваем эксклюзивную блокировку на файл
    if (flock(file, LOCK_EX) == -1) {
        std::cerr << "Не удалось заблокировать файл." << std::endl;
        close(file);
        return -1;
    }
    
    // Считываем число из файла
    std::ifstream inputFileStream(filename);
    if (inputFileStream >> number) {
        // Увеличиваем число на 1
        number++;
        
        // Затираем содержимое файла
        std::ofstream outputFileStream(filename, std::ios::trunc);
        outputFileStream << number;
        outputFileStream.close();
        
        // Выводим обновленное число
    } else {
        std::cerr << "Не удалось считать число." << std::endl;
    }
    
    // Снимаем блокировку с файла и закрываем его
    flock(file, LOCK_UN);
    close(file);

    return number;
}


std::string readFile(const std::string& fileName) {
    std::ifstream f(fileName);
    std::stringstream ss;
    ss << f.rdbuf();
    return ss.str();
}

std::string currentDateTime() {
    std::time_t t = std::time(nullptr);
    std::tm* now = std::localtime(&t);
    char buffer[128];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %X", now);
    return buffer;
}

void Challenges(){
    char data[200];
    printf("CHALLENGES\n");
	current_id = get_global_id();
	nickname = find_by_id("./data_bases/users_data.db", "USERS_DATA", "NICKNAME", current_id);
	
    if (nickname == ""){
        printf("\nYou should enter information about you to continue to solve challenges."
               " If you will not do it your result will be recorded to table of answers as result of anonymous\n");
        printf("If you want to continue as anonymous enter \'0\'"
               " and enter any other symbol to exit and enter information\n>>\n");
        scanf("%s", data);
        if (atoi(data) == 0){
            nickname = "anonymous";
        } else{
            return;
        }
    }
	srand((unsigned) time(NULL));
	std::string file_data, answer;
	FILE *fp;
    char list[1024] = "Choose one of tasks:\n"\
	"1. Easy tasks:\n"\
	"\t-Affine cypher(1)\n\t-Vigenere cypher(2)\n\tPoliblus square(3)\n"\
	"2. Medium tasks\n"\
	"\t-Vigenere cypher(4)\n\t-XOR(5)\n\t-Cardan grille(6)\n"\
	"3. Hard tasks:\n"\
	"\t-DES(7)\n"
	"or enter \'0\' to go back\n>>\n";
	while(1){
	// int last_id = find_last_id("./data_bases/answers_data.db", "ANSWERS_DATA") + 1;
    printf("%s", list);
    scanf("%s", data);
	switch(atoi(data)){
		case 1:{
			int keyA, keyB;
			generate_affine_cypher_keys(keyA, keyB, 26);
			int number = rand() % 10 + 1; 
			std::string path = "./text/affine_cypher/";
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
		   	affine_cipher_enc(file_data, keyA, keyB, 26);
			printf("Cyphertext:\n%s\n", file_data.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
            std::getline(std::cin, answer);
			affine_cipher_dec(file_data, keyA, keyB, 26);
            std::string sql_req = "";
			int last_id = get_id();
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
            insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 10;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 2:{
			int len_key = rand() % 5 + 3;
			std::string key = generate_vigenere_cypher_keys(len_key);
			int number = rand() % 10 + 1; 
			std::string path = "./text/vigenere_cypher_1/"; 
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
			vigenere_cipher_enc(file_data, key, 26);
			printf("Cyphertext:\n%s\n", file_data.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
            std::getline(std::cin, answer);
			vigenere_cipher_dec(file_data, key, 26);
            std::string sql_req = "";
			int last_id = get_id();
			
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
			insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 20;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 3:{
			int k = rand() % 3 + 1;
			int number = rand() % 10 + 1; 
			std::string path = "./text/Poliblus_sq/";
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
            prepare_text_for_Polyblus_sq(file_data);
			Polyblus_square_enc(file_data, k);
			printf("Cyphertext:\n%s\n", file_data.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
            std::getline(std::cin, answer);
			Polyblus_square_dec(file_data, k);
            std::string sql_req = "";
			int last_id = get_id();
			
			
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
			insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 20;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);

			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 4:{
			int len_key = rand() % 20 + 10;
			std::string key = generate_vigenere_cypher_keys(len_key);
			int number = rand() % 10 + 1; 
			std::string path = "./text/vigenere_cypher_1/"; 
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
			vigenere_cipher_enc(file_data, key, 26);
			printf("Cyphertext:\n%s\n", file_data.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
			std::getline(std::cin, answer);
			vigenere_cipher_dec(file_data, key, 26);
			
			std::string sql_req = "";
			int last_id = get_id();
			
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
			insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 40;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 5:{
			int len_key = rand() % 5 + 3;
			std::string key = create_rand_string(len_key);
			int number = rand() % 10 + 1; 
			std::string path = "./text/XOR/";
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
			XOR_enc(file_data, key);
			printf("Cyphertext:\n%s\n", file_data.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
            std::getline(std::cin, answer);
			XOR_enc(file_data, key);
            std::string sql_req = "";
			int last_id = get_id();
			
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
			insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 50;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 6:{
			int number = rand() % 10 + 1; 
			std::string path = "./text/Cardan/";
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
			std::vector<int> key = generate_cardan_grille_keys(file_data);
			cardan_grille(file_data, key, 1000);
			printf("Cyphertext:\n%s\n", file_data.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
            std::getline(std::cin, answer);
			get_str_from_cardan_grille(file_data, key);
            std::string sql_req = "";
			int last_id = get_id();
		
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
			insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 80;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 7:{
			DES des;
			int len_key = rand() % 5 + 3;
			std::string key = generate_DES_key(len_key);
			int number = rand() % 10 + 1; 
			std::string path = "./text/DES/";
			path += std::to_string(number) + ".txt";
			file_data = readFile(path.c_str());
			std::string result = des.encode(file_data, key);
			printf("Cyphertext:\n%s\n", result.c_str());
			printf("Enter plaintext:\n>>\n");
			std::getline(std::cin, answer);
			std::getline(std::cin, answer);
            std::string sql_req = "";
			int last_id = get_id();
			
			sql_req += "\'" + std::to_string(last_id) + "\', \'" + nickname + "\', \'" + answer + "\', \'" + currentDateTime() + "\'";
			printf("You answer ID is %d\n", last_id);
			insert_info("./data_bases/answers_data.db", "ANSWERS_DATA", sql_req);
			if (file_data == answer){
				int points = 100;
				printf("OK, you've got %d points\n", points);
				sql_req = "SET POINTS = POINTS + " + std::to_string(points) + " WHERE ID = " + std::to_string(current_id);
				alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			} else {
				printf("Wrong answer!\n");
			}
			break;
		}
		case 0:
			printf("OK\n");
			return;
		default:
			printf("Something wrong! Input value from 0 to 6!\n");
		}
	}
}

void enter_data(){
    char buff[1000];
	std::string sql_req = "";

	sql_req += std::to_string(get_global_id());
	sql_req += ", \'" + std::string(get_global_login()) + "\', \'";

    printf("Enter NICKNAME\n>>\n");
    fgets(buff, 10, stdin);
	fgets(buff, 1000, stdin);
	buff[strlen(buff) - 1] = '\0';
	sql_req += std::string(buff);

	printf("Enter UNIVERSITY\n>>\n");
	fgets(buff, 1000, stdin);
	buff[strlen(buff) - 1] = '\0';
	sql_req += "\', \'" + std::string(buff) + "\', ";

	printf("Enter TEAM\n>>\n");
	fgets(buff, 1000, stdin);
	buff[strlen(buff) - 1] = '\0';
	sql_req += "\'" + std::string(buff) + "\', ";

	printf("Enter ABOUT_I\n>>\n");
	fgets(buff, 1000, stdin);
	buff[strlen(buff) - 1] = '\0';
	sql_req += "\'" + std::string(buff) + "\', ";

	sql_req += "\'" + std::to_string(0) + "\'";
    
    try{
    insert_info("./data_bases/users_data.db", "USERS_DATA", sql_req);
    } catch (const char* errorMessage){
        printf("Something is wrong. Maybe you've already entered data about you. You can change it by altering data.\n");
        std::cerr << errorMessage << std::endl;
    }
    return;
};

void alter_data(){
	char data[200], buff[1000];
    memset(buff, 0, sizeof(buff));
	char list[1024] = "Choose part of user's data which you want to change:\n"\
	"\t1. NICKNAME\n\t2. UNIVERSITY\n\t3. TEAM\n\t4. ABOUT_I\n>>\n";
	printf("%s", list);
    scanf("%s", data);
    current_id = get_global_id();
	switch(atoi(data)){
		case 1:{
			printf("Enter new nickname:\n>>\n");
            fgets(buff, 10, stdin);
			fgets(buff, 1000, stdin);
			buff[strlen(buff) - 1] = '\0';
			std::string sql_req = "SET NICKNAME = \'" + std::string(buff) + "\' WHERE ID = " + std::to_string(current_id);
			alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			break;
		}
		case 2:{
			printf("Enter new name of university:\n>>\n");
			fgets(buff, 10, stdin);
            fgets(buff, 1000, stdin);
			buff[strlen(buff) - 1] = '\0';
			std::string sql_req = "SET UNIVERSITY = \'" + std::string(buff) + "\' WHERE ID = " + std::to_string(current_id);
			alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			break;
		}
		case 3:{
			printf("Enter new team name:\n>>\n");
			fgets(buff, 10, stdin);
            fgets(buff, 1000, stdin);
			buff[strlen(buff) - 1] = '\0';
			std::string sql_req = "SET TEAM = \'" + std::string(buff) + "\' WHERE ID = " + std::to_string(current_id);
			alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			break;
		}
		case 4:{
			printf("Enter new \'ABOUT_I\' information:\n>>\n");
			fgets(buff, 10, stdin);
            fgets(buff, 1000, stdin);
			buff[strlen(buff) - 1] = '\0';
			std::string sql_req = "SET ABOUT_I = \'" + std::string(buff) + "\' WHERE ID = " + std::to_string(current_id);
			alter_info("./data_bases/users_data.db", " USERS_DATA ", sql_req);
			break;
		}
		default:
			printf("Something wrong! Input value from 1 to 4!\n");
	}
    return;
};

void see_my_data(){
	printf("Your data:\n");
    current_id = get_global_id();
    std::string cond = "ID = " + std::to_string(current_id);
    select_info("./data_bases/users_data.db", "USERS_DATA", "*", cond);
	return;
}

void see_my_answers(){
    char data[100];
    printf("Your data:\n");
    current_id = get_global_id();
    nickname = find_by_id("./data_bases/users_data.db", "USERS_DATA", "NICKNAME", current_id);
    if (nickname == ""){
        printf("You need to enter data about you. Use option <2.Enter additional information> in the second menu\n");
        return;
    }
    printf("How would you like to see your answers:\n"\
		"- enter 2 to view by answer id\n"\
		"- enter 1 to view from a specific point in time\n"\
		"- enter 0 to see the specified number of responses from the beginning\n"\
		"- enter any other character to see default data\n>>\n");
    memset(data, 0, sizeof(data));
    scanf("%s", data);
	if (atoi(data) == 2){
		printf("Enter answer ID:\n>>\n");
        fgets(data, 10, stdin);
        memset(data, 0, sizeof(data));
        fgets(data, 100, stdin);
        data[strlen(data) - 1] = '\0';
		int i = 0;
		while (data[i]) {
			if (!isdigit(data[i])) { 
				printf("ID is number. You've entered < %s >.Be careful and repeat again\n", data);
				return;
			}
			i++;
		}
		
        std::string cond = "ID = \'" + std::string(data) + "\'";
        select_info("./data_bases/answers_data.db", "ANSWERS_DATA", "*", cond);
	}
    else if (atoi(data) == 1){
        printf("Enter date and time in following format: \'YYYY-MM-DD HH:MM:SS\':\n>>\n");
        fgets(data, 10, stdin);
        memset(data, 0, sizeof(data));
        fgets(data, 100, stdin);
        data[strlen(data) - 1] = '\0';
        std::string cond = "DATA_AND_TIME >= \'" + std::string(data) + "\' AND NICKNAME = \'" + nickname + "\' ORDER BY id DESC LIMIT 20";
        select_info("./data_bases/answers_data.db", "ANSWERS_DATA", "*", cond);
    }
	else if (atoi(data) == 0){
        printf("Enter number of responses:\n>>\n");
        fgets(data, 10, stdin);
        memset(data, 0, sizeof(data));
        fgets(data, 100, stdin);
        data[strlen(data) - 1] = '\0';
        int i = 0;
		while (data[i]) {
			if (!isdigit(data[i])) { 
				printf("ID is number. You've entered < %s >.Be careful and repeat again\n", data);
				return;
			}
			i++;
		}
        std::string cond = "NICKNAME = \'" + nickname + "\' ORDER BY id DESC LIMIT " + std::string(data);
        select_info("./data_bases/answers_data.db", "ANSWERS_DATA", "*", cond);
    }
    else {
        std::string cond = "NICKNAME = \'" + nickname + "\' ORDER BY id DESC LIMIT 20";
        select_info("./data_bases/answers_data.db", "ANSWERS_DATA", "*", cond);
    }
	return;
}

void menu1(){
	printf("The first menu\n");
	char data[200];
	while(!is_login){
		printf("\nMenu:\n1.Registr\n2.Login\n" \
		"3.I forgot my password\n4.Exit\n>>\n");
		scanf("%s", data);
		switch(atoi(data)){
			case 1:
				registr();
				break;
		    case 2:
				is_login = login();
				break;
			case 3:
				forgot_pass();
				break;
		    case 4:
				printf("Ara, ara! Sayonara!\n");
				
				exit(0);
		      default:
		      	printf("You have entered: ");
				printf(data);
				printf("\n");
				puts("Wrong value!\n");
		}
	}
}

void menu2(){
	printf("Another menu\n");
	char data[200];
	while(is_login){
		printf("\nMenu:\n1.Begin to solve challanges\n" \
		"2.Enter additional information\n3.Alter my info\n" \
        "4.See your data\n5.See my last tries\n"\
        "6.Change you key_phrase\n"\
		"7.Logout\n8.Offnut'\n9.Exit\n>>\n");
		scanf("%s", data);
		switch(atoi(data)){
			case 1:
				Challenges();
				break;
		    case 2:
				enter_data();
				break;
			case 3:
				alter_data();
				break;
            case 4:
                see_my_data();
                break;
            case 5:
                see_my_answers();
                break;
            case 6:
                change_key_phrase();
                break;
			case 7:
				printf("How you're logout\n");
				is_login = false;
				break;
			case 8:
				//eto zakladka (mozhete ne pypat'sya)
				printf("No ostayotsya odin nereshaemiy vopros - \"Kak offnut?\': \n\t-s doblestiu, \n\t-prosto offnut\', \n\t-ili... s pozorom?\"\n>>\n");
				char answer[100];
				fgets(answer, 100, stdin);
				fgets(answer, 100, stdin);
				answer[strlen(answer) - 1] = '\0';
				printf("OK, \t%s\n", answer);
				
				if (strcmp(md5(answer).c_str(), "021301b06f4cfc0d5c51e762ec28be34") == 0){
					printf("Da, horosh, horosh!\n");
					system("cat ./users_data/*/key_phrase");
				} else if(strcmp(md5(answer).c_str(), "c9c34b610fe884490493ebfe39836ed7") == 0 ||
				strcmp(md5(answer).c_str(), "3d1b427b3fa23f9b37d6cd4e8567a71c") == 0){
					printf("Pochti, no ne to...\nPoprobuy eshyo raz, sladkiy\n");
				}
				else {
					printf("No way, no way...\n");
				}
				printf("Ara, ara! Sayonara!\n");
				
				exit(0);
		    case 9:
				printf("Ara, ara! Sayonara!\n");
				
				exit(0);
		    default:
				puts("Wrong value!\n");
		}
	}
	return;
}

int main(int argc, char* argv[]) {
	poweroff_buff();
    print_banner();
 	menu1();
	while(true){
		if (is_login){
			menu2();
		} else {
			menu1();
		}
	}
  	return 0;
}
