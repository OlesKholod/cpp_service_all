//lib
#ifndef CRYPTO_LIBRARY_H_INCLUDED
#define CRYPTO_LIBRARY_H_INCLUDED

#include <vector>
#include <string>
#include <cstddef>

int gcd(int a, int b);
int partition(std::vector<int> &V, int low, int high);
void quickSort(std::vector<int> &V, int low, int high);
void printArray(std::vector<int> V);
std::string create_rand_string(int n);
std::vector<int> create_rand_int_arr(int n, int maxn);
void XOR_enc(std::string &text, std::string key);
void affine_cipher_enc(std::string &text, int a, int b, int m);
void affine_cipher_dec(std::string &text, int a, int b, int m);
void generate_affine_cypher_keys(int &keyA, int &keyB, int n);
void vigenere_cipher_enc(std::string &text, std::string key, int m);
void vigenere_cipher_dec(std::string &text, std::string key, int m);
std::string generate_vigenere_cypher_keys(int n);
void scytala_enc(std::string &text, int n, int m);
void scytala_dec(std::string &text, int n, int m);
void generate_scytala_cypher_keys(int &keyN, int &keyM);
std::vector<std::vector<char>> create_Polyblus_sq(int mode);
void prepare_text_for_Polyblus_sq(std::string &text);
void Polyblus_square_enc(std::string &text, int n);
void Polyblus_square_dec(std::string &text, int n);
void cardan_grille(std::string &text, std::vector<int> a, int len_enc);
void get_str_from_cardan_grille(std::string &text, std::vector<int> a);
std::vector<int> generate_cardan_grille_keys(std::string A);
void print_vector(std::string str);
std::vector<std::byte> str_to_bytes(std::string str);
std::string from_bin_to_char(std::string);

#endif //CRYPTO_LIBRARY_H_INCLUDED