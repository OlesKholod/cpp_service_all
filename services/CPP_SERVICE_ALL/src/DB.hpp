#ifndef DB_LIBRARY_H_INCLUDED
#define DB_LIBRARY_H_INCLUDED

#include<string>

static int callback(void* data, int argc, char** argv, char** azColName);
void create_DB(std::string namefile, std::string sql_req);
void see_BD(std::string filename, std::string tablename);
void insert_info(std::string filename, std::string tablename, std::string data);
void alter_info(std::string filename, std::string tablename, std::string data);
void select_info(std::string filename, std::string tablename, std::string data, std::string condition);
int find_last_id(std::string filename, std::string tablename);
std::string find_by_id(std::string filename, std::string tablename, std::string data, int id);

#endif //DB_LIBRARY_H_INCLUDED