#include <iostream>
#include <cstdlib>
#include <sqlite3.h>
#include <string> 
#include "DB.hpp"

const bool DEBUG = false;

static int callback(void* data, int argc, char** argv, char** azColName){
    int i;
    fprintf(stderr, "%s: ", (const char*)data);
  
    for (i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
  
    printf("\n");
    return 0;
}

void create_DB(std::string namefile, std::string sql_req){
   sqlite3 *db;
   char *zErrMsg = 0;
   int rc;
   std::string sql;

   rc = sqlite3_open_v2(namefile.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_EXCLUSIVE, nullptr);
   
   if( rc ) {
      if (DEBUG)
         fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
      sqlite3_close(db);
      return;
   } else {
      if (DEBUG)
         fprintf(stdout, "Opened database for users' data successfully\n");
   }

   rc = sqlite3_exec(db, sql_req.c_str(), callback, 0, &zErrMsg);
   
   if( rc != SQLITE_OK ){
      fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   } else {
      if (DEBUG)
         fprintf(stdout, "Table created successfully\n");
   }
   sqlite3_close(db);
   return;
}

void see_BD(std::string filename, std::string tablename){    
    sqlite3* db;
    char* messaggeError;
    int rc = sqlite3_open(filename.c_str(), &db);

     if (rc != SQLITE_OK) {
        std::cerr << "Failed to open database: " << sqlite3_errmsg(db) << std::endl;
        return;
    }
    int flags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
    rc = sqlite3_open_v2(filename.c_str(), &db, flags, NULL);
    if (rc == SQLITE_BUSY) {
        std::cerr << "Database is already opened by another user" << std::endl;
        return;
    } else if (rc != SQLITE_OK) {
        std::cerr << "Failed to open database: " << sqlite3_errmsg(db) << std::endl;
        return;
    }

    std::string query = "SELECT * FROM " + tablename + ";";
  
   //  printf("Data from TABLE %s\n", tablename.c_str());
  
    sqlite3_exec(db, query.c_str(), callback, NULL, NULL);
  
    sqlite3_close(db);
    return;
}

void insert_info(std::string filename, std::string tablename, std::string data){
    sqlite3 *db;
   	char *zErrMsg = 0;
   	int rc;
    
    std::string sql_req = "INSERT INTO " + tablename + " VALUES(" + data + ");";
     // printf("sql-query: %s", sql_req.c_str());
   	/* Open database */
   	rc = sqlite3_open_v2(filename.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_EXCLUSIVE, nullptr);
	
   	if( rc ) {
         if (DEBUG)
   	      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
   	   sqlite3_close(db);
       return;
   	} else {
         if (DEBUG)
   	      fprintf(stderr, "Opened database successfully\n");
   	}

	rc = sqlite3_exec(db, sql_req.c_str(), callback, 0, &zErrMsg);

	if( rc != SQLITE_OK ){
	   fprintf(stderr, "SQL error: %s\n", zErrMsg);
	   sqlite3_free(zErrMsg);
	} else {
      if (DEBUG)
	      fprintf(stdout, "Records created successfully\n");
	}
	sqlite3_close(db);
    return;
}

void alter_info(std::string filename, std::string tablename, std::string data){
    sqlite3 *db;
   	char *zErrMsg = 0;
   	int rc;
    std::string sql_req = "UPDATE " + tablename + " " + data;
    
   //  printf("THIS IS - %s\n", sql_req.c_str());

    rc = sqlite3_open_v2(filename.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_EXCLUSIVE, nullptr);
	
   	if( rc ) {
         if (DEBUG)
   	      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
   	   sqlite3_close(db);
       return;
   	} else {
         if (DEBUG)
   	      fprintf(stdout, "Opened database for scoreboard data successfully\n");
   	}

   	/* Execute SQL statement */
   	rc = sqlite3_exec(db, sql_req.c_str(), callback, 0, &zErrMsg);
	
   	if( rc != SQLITE_OK ){
   	   fprintf(stderr, "SQL error: %s\n", zErrMsg);
   	   sqlite3_free(zErrMsg);
   	} else {
         if (DEBUG)
   	      fprintf(stdout, "Table created successfully\n");
   	}
   	sqlite3_close(db);

    return;
}

void select_info(std::string filename, std::string tablename, std::string data, std::string condition){
    sqlite3 *db;
   	char *zErrMsg = 0;
   	int rc;
    std::string sql_req = "SELECT " + data + " FROM " + tablename + " WHERE " + condition;
    
   // std::cout << sql_req << std::endl;

    rc = sqlite3_open_v2(filename.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_EXCLUSIVE, nullptr);
	
   	if( rc ) {
         if (DEBUG)
   	      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
   	   sqlite3_close(db);
       return;
   	} else {
         if (DEBUG)
   	      fprintf(stdout, "Opened database for scoreboard data successfully\n");
   	}

   	/* Execute SQL statement */
   	rc = sqlite3_exec(db, sql_req.c_str(), callback, 0, &zErrMsg);
	
   	if( rc != SQLITE_OK ){
   	   fprintf(stderr, "SQL error: %s\n", zErrMsg);
   	   sqlite3_free(zErrMsg);
   	} else {
         if (DEBUG)
   	      fprintf(stdout, "Table created successfully\n");
   	}
   	sqlite3_close(db);

    return;
}

int find_last_id(std::string filename, std::string tablename){
   sqlite3* db;
   char *zErrMsg = 0;
   int rc;
   rc = sqlite3_open_v2(filename.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_EXCLUSIVE, nullptr);
    
   if( rc != SQLITE_OK) {
      if (DEBUG)
         fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
      sqlite3_close(db);
      return 0;
   } else {
      if (DEBUG)
         fprintf(stdout, "Opened database for scoreboard data successfully\n");
   }

   int last_id;

   std::string sql_req = "SELECT id FROM " + tablename + " ORDER BY id DESC LIMIT 1";
   // std::cout << sql_req << std::endl;
   sqlite3_stmt* statement;
   if (sqlite3_prepare_v2(db, sql_req.c_str(), -1, &statement, NULL) == SQLITE_OK) {
      if (sqlite3_step(statement) == SQLITE_ROW) {
         last_id = sqlite3_column_int(statement, 0);
      }
   }

   sqlite3_finalize(statement);
   sqlite3_close(db);
   return last_id;
}

std::string find_by_id(std::string filename, std::string tablename, std::string data, int id){
   std::string result;
    sqlite3* db;
    sqlite3_stmt* stmt;
    
    int rc = sqlite3_open_v2(filename.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_EXCLUSIVE, nullptr);
    
    if (rc != SQLITE_OK) {
        std::cerr << "Error opening database: " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
        return result;
    }
    
    std::string query = "SELECT " + data + " FROM " + tablename + " WHERE id = " + std::to_string(id);
    
    rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    
    if (rc != SQLITE_OK) {
        std::cerr << "Error while preparing request: " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
        return result;
    }
    
    rc = sqlite3_step(stmt);
    
    if (rc == SQLITE_ROW) {
        const unsigned char* dataResult = sqlite3_column_text(stmt, 0);
        result = reinterpret_cast<const char*>(dataResult);
    }
    
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    return result;
} 

void setup_BD(){
    return;
}