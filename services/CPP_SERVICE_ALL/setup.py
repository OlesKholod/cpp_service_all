import sqlite3
import subprocess

def main():

    subprocess.run("cd libs && make clean && make && cd ..", shell=True)

    cmd = """
    mkdir users_data && cd users_data && mkdir default && cd default && 
    echo '1' > id && echo 'default' > password && echo 'default' > key_phrase && 
    cd ../.. && echo '2' > ./src/last_id.txt && echo '0' > ./src/answer_id.txt && mkdir data_bases
    """
    subprocess.run(cmd, shell=True)

    conn = sqlite3.connect('./data_bases/users_data.db')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE USERS_DATA(
                    ID INT PRIMARY KEY     NOT NULL,
                    LOGIN   TEXT  NOT NULL,
                    NICKNAME           TEXT    NOT NULL,
                    UNIVERSITY        TEXT,
                    TEAM         TEXT,
                    ABOUT_I   MEDIUMTEXT,
                    POINTS   INT)''')
    cursor.execute('''
    INSERT INTO USERS_DATA (ID, LOGIN, NICKNAME, UNIVERSITY, TEAM, ABOUT_I, POINTS) 
    VALUES (1, 'default', 'default_user', 'default', 'default', 'd', 0)
    ''')
    conn.commit()

    conn.close()

    conn = sqlite3.connect('./data_bases/answers_data.db')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE ANSWERS_DATA(
                    ID INT PRIMARY KEY  AUTOINCREMENT   NOT NULL,
                    NICKNAME           TEXT    NOT NULL,
                    ANSWER   TEXT  NOT NULL,
                    DATA_AND_TIME  DATETIME)''')
    cursor.execute('''
    INSERT INTO ANSWERS_DATA (ID, NICKNAME, ANSWER, DATA_AND_TIME) 
    VALUES (0, 'default', 'default_user', '2023-01-30 00:00:00')
    ''')            
    conn.commit()
    conn.close()

    return

if __name__ == "__main__":
    main()


# import sqlite3

# print("========================ANSWER_DATA==========================")
# # Подключение к базе данных answers_data.db
# conn = sqlite3.connect("./answers_data.db")
# cursor = conn.cursor()

# # Выполнение запроса на выборку всех данных
# cursor.execute("SELECT * FROM ANSWERS_DATA")

# # Вывод результатов запроса
# rows = cursor.fetchall()
# for row in rows:
#     print(row)

# # Закрытие соединения
# conn.close()