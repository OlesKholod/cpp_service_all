# Training service

Service for A/D CTF training on C++.

## service

Service for solving cryptopals (cryptographic tasks).

### Tags

- C++
- sqlite3
- libssl

### Vulnerabilities

- the ability to perform of sql injection, since the program does not check the arguments before forming a query to the database [EXPLOIT](./sploits/CPP_SERVICE_ALL/super_sql_exp.py).

- the ability to carry out a rop attack due to uncontrolled buffer filling (using the scanf () function of the C programming language) and the ability to view the contents of the stack due to a format string vulnerability [EXPLOIT](./sploits/CPP_SERVICE_ALL/super_rop_exp.py).

## Deploy

### Service

```
cd ./service # or cd ./services/CPP_SERVICE_ALL
docker-compose up -d --build #add --force-recreate for rebuilding of container if you have changed something in service's files.
``` 

### Checker

The checker interface matches the description for ructf: https://github.com/HackerDom/ructf-2017/wiki/Интерфейс-«проверяющая-система-чекеры»

```
export RUN=150
export SERVICE=all
./check.py up
./check.py check
./check.py down
```
